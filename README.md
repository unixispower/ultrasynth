# Ultrasound Synthesizer
Python script that generates audio from a spectral Doppler image (or other
spectrogram of white noise).

The input for this script is an image of a spectral Doppler like that produced
by an ultrasound machine:

![Spectral Doppler image of a human heartbeat](spectrogram.png)

The output is a WAV file:

[Audio synthesized from above image](output.wav)

See the [full project](https://blaines.world/projects/ultrasynth.html) for more
details.


## Requirements
This utility is a [Python 3](https://www.python.org/) script that uses the
[Pillow library](https://python-pillow.org/).


## Usage
If the input has been processed and is relatively free of noise, something like
the following should produce decent results:

```shell
./ultrasynth spectrogram.png --length-seconds 5.2
```

If your input image is noisy, some background noise can be added to mask image
artifacts:

```shell
./ultrasynth spectrogram.png output-noise.wav -l 5.2 --minimum-amplitude 0.025
```

If your output is too loud or too quiet, try adjusting the output gain (the
default is 1):

```shell
./ultrasynth spectrogram.png output-loud.wav -l 5.2 --volume=1.5
```

The default high frequency is 2500Hz to mimic an ultrasound machine, but this
can be changed:

```shell
./ultrasynth spectrogram.png output-4khz-range.wav -l 5.2 --high-freq=4000
```

As well as the output sample rate (the default is 8kHz):

```shell
./ultrasynth spectrogram.png output-16khz-rate.wav -l 5.2 --sample-rate=16000
```


## Licensing
Source in this repository is licensed under the 2-clause BSD license, see
`LICENSE` for details.
